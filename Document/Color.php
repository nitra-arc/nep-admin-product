<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="Colors")
 */
class Color
{
    use \Nitra\ProductBundle\Traits\AliasDocument;
    use \Nitra\StoreBundle\Traits\LocaleDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Название
     * @ODM\String
     * @Assert\NotBlank
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Цвет (16-ричная запись (000000 - черный))
     * @ODM\String
     */
    protected $color;

    /**
     * @var string Путь к изображению
     * @ODM\String
     */
    protected $image;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     * @param string $color
     * @return self
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Get color
     * @return string $color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }
}
# ProductBundle

## Конфигурация
### SluggableListener
Для включения nitra транслитерации - пропишите в config.yml:

```yaml
    stof_doctrine_extensions:
        class:
            sluggable: Nitra\ProductBundle\Sluggable\SluggableListener
```

### Конфигурация (config.yml)
По умолчанию:

```yaml
    nitra_product:
        auto_product_article: false
        search_listener:
            fields:
                - [ getCategory, getName ]
                - [ getBrand, getName ]
                - getModel
                - getName
                - getArticle
            instanceof:
                'category': '\Nitra\ProductBundle\Document\Category'
                'brand': '\Nitra\ProductBundle\Document\Brand'
```

* **auto_product_article** - способ формирования артикула (false - ручной, true - автоматический индекс)
* **search_listener** - настройки формирования поискового поля
    * **fields** - геттеры для формирования строки поиска
    * **instanceof** - документы при редактировании (создании) которых должно обновляться поле поиска

```yaml
    nitra_product:
      search_listener:
        fields: // геттеры
          - "get...()"
          - ...
        instanceof: // документы
          'field from product document': 'document namespace'
          '...': '...'
```

**field from product document** - поле для выборки товаров (к примеру 'category', туда доклеивается '.$id' для выборки всех товаров этой категории)


### Настройки (parameters.yml)

```yaml
    parameters:
        #...
        locale: ru
        locales: [ ru, en ]
        #...
```

* **sluggable_symbols_replacer** - замена символов при формировании алиасов для товаров, категорий....
* **locale** - локаль по умолчанию
* **locales** - доступный локали для перевода
<?php

namespace Nitra\ProductBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ResaveProductsCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nitra:resave:products')
            ->setDescription('Resaving all products')
            ->addArgument('package', InputArgument::OPTIONAL, 'group of 1000 prods -1 to all', -1)
            ->addArgument('product-repository', InputArgument::OPTIONAL, 'Repository of product', 'NitraProductBundle:Product')
            ->addOption('batch-size', 'b', InputOption::VALUE_OPTIONAL, 'Count products group for flushing', 200);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $s = microtime(true);
        // пересохранение товаров
        $products = $this->getProducts($input->getArgument('product-repository'), (int) $input->getArgument('package'));

        $i = 0;
        $proggress = $this->getHelperSet()->get('progress');
        $proggress->start($output, $products->count());
        $proggress->setBarWidth(80);
        foreach ($products as $product) {
            $product->setAliasEn(null);
            if ((($i % $input->getOption('batch-size')) == 0) && ($i != 0)) {
                $this->flush($output);
            }
            ++ $i;
            $proggress->advance();
        }
        $proggress->finish();
        $this->flush($output);
        $output->writeln('Resaved ' . ($this->formatBoldRed($i)) . ' products');
        $output->writeln("Resaving completed ({$this->formatBoldRed($this->niceTime(microtime(true) - $s))}).");
    }

    /**
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function flush($output)
    {
//        var_dump($output->getVerbosity());die;
        $start      = microtime(true);
        $this->getDocumentManager()->flush();
        $flushing   = microtime(true) - $start;
        $mS         = memory_get_usage();
        $this->getDocumentManager()->clear();
        if (OutputInterface::VERBOSITY_NORMAL < $output->getVerbosity()) {
            $output->writeln("\n"
                . "flushing: {$this->formatBoldRed($this->niceTime($flushing))}\n"
                . "memory:\n"
                . "    before clear: {$this->formatBoldRed($this->niceSize($mS))}\n"
                . "    after clear: {$this->formatBoldRed($this->niceSize(memory_get_usage()))}\n"
                . "    cleared: {$this->formatBoldRed($this->niceSize($mS - memory_get_usage()))}\n"
            );
        }
    }

    /**
     * @param string $repo
     * @param int $group
     * @return \Doctrine\MongoDB\Cursor
     */
    protected function getProducts($repo, $group)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder($repo)
            ->sort('_id', 'asc');
        if ($group != -1) {
            $qb->limit(1000)->skip(1000 * $group);
        }

        return $qb->eagerCursor(true)->getQuery()->iterate();
    }
}
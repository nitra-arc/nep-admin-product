<?php
namespace Nitra\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ResaveParametersCommand extends ContainerAwareCommand
{
    
    protected function configure() {
        $this
            ->setName('nitra:resave:parameters')
            ->setDescription('Resaving all parameters and their values')
            ->addArgument('parameter-repository', InputArgument::OPTIONAL, 'Repository of parameter', 'NitraProductBundle:Parameter');
    }
	
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        // пересохранение товаров
        $parameter_repo = $dm->getRepository($input->getArgument('parameter-repository'));
        $parameters = $parameter_repo->findAll();
        $i = 0;
        foreach ($parameters as $parameter) {
            $parameter->setAliasEn(null);
            
            foreach ($parameter->getParameterValues() as $value) {
                $value->setAliasEn(null);
            }
            
            if ($i%200 == 0) {
                $dm->flush();
            }
            $i++;
        }
        $output->writeln('Resaved ' . ($i) . ' parameters');
        $dm->flush();
        $output->writeln('Resaving completed.');
    }
}
<?php

namespace Nitra\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ResaveCategoriesCommand extends ContainerAwareCommand
{
    
    protected function configure() {
        $this
            ->setName('nitra:resave:categories')
            ->setDescription('Resaving all categories')
            ->addArgument('category-repository', InputArgument::OPTIONAL, 'Repository of category', 'NitraProductBundle:Category')
            ->addOption('regenerate-path', 'p', InputOption::VALUE_NONE, 'Regenerate gedmo paths (and levels) for categories', null);
    }
	
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $rPath = $input->getOption('regenerate-path');
        // пересохранение
        $categryRepo = $dm->getRepository($input->getArgument('category-repository'));
        $categories = $categryRepo->findAll();
        
        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, $categories->count());
        
        $i = 0;
        foreach ($categories as $category) {
            $category->setAliasEn(null);
            if ($rPath) {
                $parent = $category->getParent() ? ($category->getParent()->getId()) : null;
                $category->removeParent();
                $dm->flush();
                if ($parent) {
                    $category->setParent($categryRepo->find($parent));
                }
            }
            
            if (($i%200 == 0) || $rPath) {
                $dm->flush();
            }
            $i++;
            $progress->advance();
        }
        $progress->finish();
        
        $output->writeln('Resaved ' . ($i) . ' categories');
        $dm->flush();
        $output->writeln('Resaving completed.');
    }
}
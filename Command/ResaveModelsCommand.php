<?php

namespace Nitra\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ResaveModelsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nitra:resave:models')
            ->setDescription('Resaving models')
            ->addArgument('package', InputArgument::OPTIONAL, 'group of 1000 models, -1 to all', -1)
            ->addArgument('model-repository', InputArgument::OPTIONAL, 'Repository of model', 'NitraProductBundle:Model');
    }
	
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // пересохранение товаров
        $group = (int) $input->getArgument('package');
        
        $dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $qb = $dm->createQueryBuilder($input->getArgument('model-repository'));
        if ($group != -1) {
            $qb->limit(1000)->skip(1000 * $group);
        }
        $models = $qb->getQuery()->execute();

        $i = 0;
        $proggress = $this->getHelperSet()->get('progress');
        $proggress->start($output, $models->count());
        $proggress->setBarWidth(80);
        
        foreach ($models as $model) {
            $model->setAlias(null);
            if ($i % 200 == 0) {
                $dm->flush();
            }
            $i++;
            $proggress->advance();
        }
        $proggress->finish();
        
        $output->writeln('Resaved ' . ($i) . ' models');
        $dm->flush();
        $output->writeln('Resaving completed.');
    }
}
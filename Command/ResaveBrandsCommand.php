<?php

namespace Nitra\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ResaveBrandsCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('nitra:resave:brands')
            ->setDescription('Resaving all brands')
            ->addArgument('brand-repository', InputArgument::OPTIONAL, 'Repository of brand', 'NitraProductBundle:Brand');
    }
	
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        // пересохранение товаров
        $brand_repo = $dm->getRepository($input->getArgument('brand-repository'));
        $brands = $brand_repo->findAll();
        $i = 0;
        foreach ($brands as $brand) {
            $brand->setAliasEn(null);
            
            if ($i%200 == 0) {
                $dm->flush();
            }
            $i++;
        }
        $output->writeln('Resaved ' . ($i) . ' brands');
        $dm->flush();
        $output->writeln('Resaving completed.');
    }
}
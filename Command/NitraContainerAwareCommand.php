<?php

namespace Nitra\ProductBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class NitraContainerAwareCommand extends ContainerAwareCommand
{
    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * @param string $str
     * @return string
     */
    protected function formatBoldRed($str)
    {
        return "\033[1;31m$str\033[0m";
    }

    /**
     * @param int $size
     * @return string
     */
    protected function niceSize($size)
    {
        return $this->niceX($size, array('b', 'Kb', 'Mb', 'Gb', 'Tb'), 1024);
    }
    
    /**
     * @param int $sec
     * @return string
     */
    protected function niceTime($sec)
    {
        return $this->niceX($sec, array('sec.', 'min.', 'hours.', 'days.', 'years.'), 60);
    }
    
    /**
     * @param int $val
     * @param array $iec
     * @param int $ir
     * @param int $round
     * @return string
     */
    protected function niceX($val, $iec, $ir, $round = 4)
    {
        $i = 0;
        while (($val / $ir) > 1) {
            $val = $val / $ir;
            $i++;
        }
        return round($val, $round) . ' ' . $iec[$i];
    }
}
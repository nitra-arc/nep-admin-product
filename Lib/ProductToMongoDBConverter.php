<?php

namespace Nitra\ProductBundle\Lib;

class ProductToMongoDBConverter
{
    /**
     * @param object $object
     * @return array
     */
    protected function convertObjectToDBRef($object)
    {
        return array(
            '$ref'  => $this->dm->getRepository(get_class($object))->getClassMetadata()->collection,
            '$id'   => new \MongoId($object->getId()),
            '$db'   => $this->dm->getConfiguration()->getDefaultDB(),
        );
    }

    /**
     * @param \Nitra\ProductBundle\Document\Badge|null $value
     */
    protected function convertBadgeToDatabaseValue($value)
    {
        $field = 'badge';
        if (!$value) {
            return array($field, null);
        }

        return array(
            $field,
            $this->convertObjectToDBRef($value),
        );
    }

    /**
     * @param float $value
     * @return array
     */
    protected function convertCurentStorePriceToDatabaseValue($value)
    {
        return array(
            'storePrice.' . $this->getStoreId() . '.price',
            $value,
        );
    }

    /**
     * @param float $value
     * @return array
     */
    protected function convertCurentStoreDiscountToDatabaseValue($value)
    {
        return array(
            'storePrice.' . $this->getStoreId() . '.discount',
            $value,
        );
    }

    /**
     * @param boolean $value
     * @return array
     */
    protected function convertIsFixedPriceToDatabaseValue($value)
    {
        return array(
            'storePrice.' . $this->getStoreId() . '.isFixedPrice',
            $value,
        );
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product[] $value
     * @return array
     */
    protected function convertAccessoriesToDatabaseValue($value)
    {
        $refs = array();

        foreach ($value as $accessorie) {
            $refs[] = $this->convertObjectToDBRef($accessorie);
        }

        return array(
            'accessories',
            $refs,
        );
    }

    /**
     * @param \Nitra\StoreBundle\Document\Store[] $value
     * @return array
     */
    protected function convertStoresToDatabaseValue($value)
    {
        $refs = array();

        foreach ($value as $store) {
            $refs[] = $this->convertObjectToDBRef($store);
        }

        return array(
            'stores',
            $refs,
        );
    }
}
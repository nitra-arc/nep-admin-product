<?php

namespace Nitra\ProductBundle\Controller\Product;

use Admingenerated\NitraProductBundle\BaseProductController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ListController extends BaseListController
{
    /**
     * @param \Doctrine\MongoDB\Query\Builder $query
     */
    protected function processFilters($query)
    {
        parent::processFilters($query);

        $filterObject = $this->getFilters();

        $qArray = $query->getQueryArray();
        if (key_exists('status', $qArray)) {
            unset($qArray['status']);
        }
        $query->setQueryArray($qArray);

        if (isset($filterObject['status']) && $filterObject['status']) {
            $query->field('status')->equals(new \MongoRegex('/deleted|.*/'));
        }
    }

    /**
     * @Route("/get-group-autocomplete", name="Nitra_ProductBundle_Product_Model_Autocomplete")
     */
//    public function modelAutocomplete(Request $request)
//    {
//        $q                  = $request->get('term');
//        $autocompleteReturn = array();
//
//        $words              = preg_split('/\s+/', trim($q));
//
//        $qb                 = $this->getDocumentManager()
//            ->createQueryBuilder('NitraProductBundle:Product');
//
//        foreach ($words as $word) {
//            if ($word != null) {
//                $qb->addAnd($qb->expr()->field('group')->equals(new \MongoRegex('/' . trim($word) . '/i')));
//            }
//        }
//
//        $groups = $qb
//            ->limit(20)
//            ->distinct('group')
//            ->getQuery()
//            ->execute();
//
//        foreach ($groups as $group) {
//            $autocompleteReturn[] = $group;
//        }
//
//        return new Response(json_encode($autocompleteReturn));
//    }

    /**
     * Функция для поиска неподвязанных аксессуаров по категории
     * @Route("/find-unselected_accessories", name="Nitra_ProductBundle_findUnselectedAccessoriesByCategory")
     */
    public function findUnselectedAccessoriesByCategory(Request $request)
    {
        $dm = $this->getDocumentManager();
        $category = $request->query->get('category');
        $selected = $request->query->get('products');

        $models   = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('id')
            ->field('category.id')->equals($category)
            ->getQuery()->execute()->toArray();

        $qb       = $dm
            ->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in(array_keys($models));

        if (!empty($selected)) {
            $qb->field('id')->notIn(array_values($selected));
        }

        $products = $qb->getQuery()->execute();

        $response = array();
        foreach ($products as $value) {
            $response[$value->getId()] = $value->getModel() . ' ' . $value->getName();
        }

        return new Response(json_encode($response));
    }

    /**
     * Load colors for product
     * @Route("/load_colors", name="Nitra_ProductBundle_loadColors")
     */
    public function loadColors(Request $request)
    {
        $dm        = $this->getDocumentManager();
        $response  = array();
        $colorsIds = $request->request->get('colors_ids');
        if ($colorsIds) {
            foreach ($colorsIds as $colorId) {
                if ($colorId) {
                    $color = $dm->find('NitraProductBundle:Color', $colorId);
                    $response[] = array(
                        'id'    => $colorId,
                        'color' => $color->getColor(),
                        'image' => $color->getImage(),
                    );
                }
            }
        }

        return new JsonResponse($response);
    }
}
<?php

namespace Nitra\ProductBundle\Controller\Parameter;

use Admingenerated\NitraProductBundle\BaseParameterController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class ListController extends BaseListController
{
    /**
     * @Route("/get-group-autocomplete", name="Nitra_ProductBundle_Parameter_Group_Autocomplete")
     */
    public function groupAutocomplete()
    {
        $q                  = $this->getRequest()->get('term');
        $autocompleteReturn = array();

        $words              = preg_split('/\s+/', trim($q));

        $qb                 = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Parameter');

        foreach ($words as $word) {
            if ($word != null) {
                $qb->addAnd($qb->expr()->field('group')->equals(new \MongoRegex('/' . trim($word) . '/i')));
            }
        }

        $groups = $qb
            ->limit(20)
            ->distinct('group')
            ->getQuery()
            ->execute();

        foreach ($groups as $group) {
            $autocompleteReturn[] = $group;
        }

        return new Response(json_encode($autocompleteReturn));
    }
}
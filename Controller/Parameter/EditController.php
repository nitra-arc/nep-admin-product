<?php

namespace Nitra\ProductBundle\Controller\Parameter;

use Admingenerated\NitraProductBundle\BaseParameterController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /**
     * @param \Symfony\Component\Form\Form              $form The valid form
     * @param \Nitra\ProductBundle\Document\Parameter   $parameter
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\ProductBundle\Document\Parameter $parameter)
    {
        $this->removeValuesFromProducts($parameter);
    }

    /**
     * @param \Symfony\Component\Form\Form              $form
     * @param \Nitra\ProductBundle\Document\Parameter   $parameter
     */
    public function postSave(\Symfony\Component\Form\Form $form, $parameter)
    {
        if ($this->container->hasParameter('locales') && $this->container->getParameter('locales')) {
            $this->fillTranslationsOfValues($form);
        }
    }

    /**
     * Fill values translations
     * @param \Symfony\Component\Form\Form $form
     */
    protected function fillTranslationsOfValues($form)
    {
        $locales = $this->container->getParameter('locales');

        $postData = $this->getRequest()->request->get($form->getName())['parameterValues'];
        $data = array();
        foreach ($postData as $item) {
            $data[$item['name']] = $item;
        }
        foreach ($form->get('parameterValues')->getData() as $value) {
            $valueData = $data[$value->getName()];
            foreach ($valueData as $field => $val) {
                if (in_array($field, $locales) && $val) {
                    $trans = $this->findTrans($value->getId(), $field);
                    $trans->setContent($val);
                    $this->getDocumentManager()->flush();
                }
            }
        }
    }

    /**
     * @param string $valueId
     * @param string $locale
     * @return string
     */
    protected function findTrans($valueId, $locale)
    {
        $data = $this->getDocumentManager()->createQueryBuilder('Gedmo\Translatable\Document\Translation')
            ->field('foreignKey')->equals($valueId)
            ->field('objectClass')->equals('Nitra\\ProductBundle\\Document\\ParameterValue')
            ->sort('locale', 'asc')
            ->getQuery()->execute()->toArray();

        if ($data && count($data)) {
            foreach ($data as $row) {
                if (($row->getField() == 'name') && ($row->getLocale() == $locale)) {
                    return $row;
                }
            }
        }

        $trans = new \Gedmo\Translatable\Document\Translation();
        $trans->setField('name')
            ->setObjectClass('Nitra\\ProductBundle\\Document\\ParameterValue')
            ->setForeignKey($valueId)
            ->setLocale($locale);

        $this->getDocumentManager()->persist($trans);

        return $trans;
    }

    /**
     * Remove deleted values from products
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     */
    protected function removeValuesFromProducts($parameter)
    {
        $existsValues = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:ParameterValue')
            ->field('parameter')->references($parameter)
            ->getQuery()->execute()->toArray();

        $currentValuesIds = array();
        foreach ($parameter->getParameterValues() as $currentValue) {
            $currentValuesIds[] = $currentValue->getId();
        }

        $valuesIdsToRemove = array();
        foreach ($existsValues as $id => $value) {
            if (!in_array($id, $currentValuesIds)) {
                $valuesIdsToRemove[] = new \MongoId($id);
                $this->getDocumentManager()->remove($value);
            }
        }

        if (!$valuesIdsToRemove) {
            return;
        }

        $this->removeValuesFromProductsByIds($valuesIdsToRemove);
    }

    /**
     * Remove values from products by ids
     * @param array $ids
     */
    protected function removeValuesFromProductsByIds($ids)
    {
        // initial query builder
        $qb = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Product')
            ->update()->multiple();

        // add condition on product has removed value
        $qb->field('parameters')->elemMatch($qb->expr()
            ->field('values')->in($ids)
        );

        // pull value from products
        $qb->field('parameters.$.values')->pull($qb->expr()
            ->in($ids)
        );

        $qb->getQuery()->execute();
    }
}
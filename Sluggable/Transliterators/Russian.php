<?php

namespace Nitra\ProductBundle\Sluggable\Transliterators;

use Gedmo\Sluggable\Util\Urlizer;

class Russian extends Urlizer
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected static $container = null;
    
    public static $lits = array(
        "a"     => "а",
        "b"     => "б",
        "v"     => "в",
        "g"     => "г",
        "d"     => "д",
        "e"     => "е",
        "yo"    => "ё",
        "zh"    => "ж",
        "z"     => "з",
        "i"     => "и",
        "y"     => "й",
        "k"     => "к",
        "l"     => "л",
        "m"     => "м",
        "n"     => "н",
        "o"     => "о",
        "p"     => "п",
        "r"     => "р",
        "s"     => "с",
        "t"     => "т",
        "u"     => "у",
        "f"     => "ф",
        "h"     => "х",
        "c"     => "ц",
        "ch"    => "ч",
        "sh"    => "ш",
        "sh"    => "щ",
        "y"     => "ы",
        "e"     => "э",
        "yu"    => "ю",
        "ya"    => "я",
   
        "A"     => "А",
        "B"     => "Б",
        "V"     => "В",
        "G"     => "Г",
        "D"     => "Д",
        "E"     => "Е",
        "Yo"    => "Ё",
        "Zh"    => "Ж",
        "Z"     => "З",
        "I"     => "И",
        "Y"     => "Й",
        "K"     => "К",
        "L"     => "Л",
        "M"     => "М",
        "N"     => "Н",
        "O"     => "О",
        "P"     => "П",
        "R"     => "Р",
        "S"     => "С",
        "T"     => "Т",
        "U"     => "У",
        "F"     => "Ф",
        "H"     => "Х",
        "C"     => "Ц",
        "Ch"    => "Ч",
        "Sh"    => "Ш",
        "Sh"    => "Щ",
        "Y"     => "Ы",
        "E"     => "Э",
        "Yu"    => "Ю",
        "Ya"    => "Я",
    );
    
    /**
     * Uses transliteration tables to convert any kind of utf8 character
     *
     * @param string $text
     * @param string $separator
     * @return string $text
     */
    public static function transliterate($text, $separator = '-', $object = null)
    {
        $as = array();
        if (self::$container && self::$container->hasParameter('sluggable_symbols_replacer')) {
            $as = self::$container->getParameter('sluggable_symbols_replacer');
        }
        if ($as) {
            $replacemend = array_map(function($r) { return '/' . preg_replace('/([^A-Za-z])/', '\\\$1', $r) . '/'; }, array_keys($as));
            $replaced    = array_values($as);
            $text        = preg_replace($replacemend, $replaced, $text);
        }
        
        $text = strtr($text, self::$lits);
        $text = preg_replace("`\[.*\]`U", "", $text);
        $text = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', $separator, $text);
        $text = htmlentities($text, ENT_COMPAT, 'utf-8');
        $text = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $text);
        $text = preg_replace(array("`[^а-яё0-9]`iu", "`[-]+`"), $separator, $text);

        return mb_strtolower(trim($text, $separator), 'utf-8');
    }

    public static function urlize($text, $separator = '-')
    {
        return $text;
    }
}
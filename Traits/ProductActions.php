<?php

namespace Nitra\ProductBundle\Traits;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\RedirectResponse;

trait ProductActions
{
    /**
     * Process delete product
     * @param \Nitra\ProductBundle\Document\Product $Product
     */
    protected function executeObjectDelete($Product)
    {
        $Product->setStatus('deleted');
        $Product->setArticle($Product->getArticle() . '/deleted');
        $dm = $this->getDocumentManager();
        $dm->flush();
        $dm->clear();
    }

    /**
     * Attempt copy product
     * @param string $pk Product id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function attemptObjectCopy($pk)
    {
        $Product = $this->getObject($pk);
        try {
            $this->executeObjectCopy($Product);
            return $this->successObjectCopy($Product);
        } catch (\Exception $e) {
            return $this->errorObjectCopy($e, $Product);
        }
    }

    /**
     * On success copy process
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function successObjectCopy($Product)
    {
        $message = $this->trans('actions.copy.success', array(
            '%name%' => (string) $Product,
        ));

        $this->get('session')->getFlashBag()->add('success', $message);

        $route = isset($this->listRoute)
            ? $this->listRoute
            : 'Nitra_ProductBundle_Product_list';

        return new RedirectResponse($this->generateUrl($route));
    }

    /**
     * On error copy process
     * @param \Exception $e
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function errorObjectCopy(\Exception $e, $Product = null)
    {
        $message = $this->trans('actions.copy.error', array(
            '%name%' => (string) $Product,
        ));

        $this->get('session')->getFlashBag()->add('error', $message);

        $route = isset($this->listRoute)
            ? $this->listRoute
            : 'Nitra_ProductBundle_Product_list';

        return new RedirectResponse($this->generateUrl($route));
    }

    /**
     * Translate message
     * @param string $trans
     * @param array  $options
     * @return string
     */
    protected function trans($trans, array $options = array())
    {
        return $this->get('translator')->trans($trans, $options, 'NitraProductBundleProduct');
    }

    /**
     * Process copy product
     * @param \Nitra\ProductBundle\Document\Product $hydratedProduct
     */
    protected function executeObjectCopy($hydratedProduct)
    {
        // get product as array
        $product = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)
            ->field('id')->equals($hydratedProduct->getId())
            ->getQuery()->execute()->getSingleResult();

        // remove special fields
        unset(
            $product['_id'],
            $product['article'],
            $product['counter'],
            $product['fullUrlAliasEn'],
            $product['fullNameForSearch']
        );

        // remove ignore fields
        $this->unsetIgnoredFields($product);

        // clone product parameters
        $this->cloneParameters($product);

        // clone product image, image2, images
        $this->cloneImages($product);

        // reset created at date
        $product['createdAt'] = new \MongoDate();
        // reset updated at date
        $product['updatedAt'] = new \MongoDate();

        // insert product to database
        $this->insertProduct($product);
    }

    /**
     * Unset congigured ignore fields
     * @param array $product
     */
    protected function unsetIgnoredFields(&$product)
    {
        // get store id
        $storeId = $this->get('session')->get('store_id', null);
        // get store by id
        $store   = $this->getDocumentManager()->find('NitraStoreBundle:Store', $storeId);

        // foreach by configured ignore fields
        foreach ($store->getProductManagement()->getIgnoreCopyFields() as $field) {
            // if field in storePrice array
            if (in_array($field, array('curentStorePrice', 'curentStoreDiscount', 'isFixedPrice'))) {
                // modify virtual field to storePrice field
                $storeField = lcfirst(str_replace('curentStore', '', $field));
                // if field is defined for product
                if (array_key_exists('storePrice', $product) &&
                    array_key_exists($storeId, $product['storePrice']) &&
                    array_key_exists($storeField, $product['storePrice'][$storeId])
                ) {
                    // unset
                    unset($product['storePrice'][$storeId][$storeField]);
                    // if storePrice for current store is empty
                    if (!$product['storePrice'][$storeId]) {
                        // unset
                        unset($product['storePrice'][$storeId]);
                    }
                }
            // else if field is defined (description, parameters, isActive, etc)
            } elseif (array_key_exists($field, $product)) {
                // unset
                unset($product[$field]);
            }
        }
    }

    /**
     * Clone product parameters
     * @param array $product
     */
    protected function cloneParameters(&$product)
    {
        // if parameters is defined
        if (array_key_exists('parameters', $product)) {
            foreach ($product['parameters'] as &$parameter) {
                // reset uid
                $parameter['_id']       = new \MongoId();
                // reset created at date
                $parameter['createdAt'] = new \MongoDate();
                // reset updated at date
                $parameter['updatedAt'] = new \MongoDate();
            }
        }
    }

    /**
     * Clone product image, image2 and images fields
     * @param array $product
     */
    protected function cloneImages(&$product)
    {
        // if image is defined and exists, clone him and set
        if (array_key_exists('image', $product) && ($newImage = $this->cloneImage($product['image']))) {
            $product['image'] = $newImage;
        }

        // if image2 is defined and exists, clone him and set
        if (array_key_exists('image2', $product) && ($newImage = $this->cloneImage($product['image2']))) {
            $product['image2'] = $newImage;
        }

        // if images is defined
        if (array_key_exists('images', $product)) {
            // define new images array
            $newImages = array();
            // fill new images as cloned olest
            foreach ($product['images'] as $image) {
                if ($newImage = $this->cloneImage($image)) {
                    $newImages[] = $newImage;
                }
            }
            // if new images
            if ($newImages) {
                // set him
                $product['images'] = $newImages;
            }
        }
    }

    /**
     * Clone one image if exists
     * @param string $image Path to source image
     * @return string Path to cloned image
     */
    protected function cloneImage($image)
    {
        $pathToWeb = realpath(
            $this->get('kernel')->getRootDir() .
            DIRECTORY_SEPARATOR .
            '..' .
            DIRECTORY_SEPARATOR .
            'web'
        );
        if (!file_exists($pathToWeb . $image)) {
            return;
        }

        $newImage = preg_replace('/(.*\/).*\.(.*)$/', '${1}' . uniqid() . '.${2}', $image);

        $filesystem = new Filesystem();
        $filesystem->copy($pathToWeb . $image, $pathToWeb . $newImage);

        return $newImage;
    }

    /**
     * Insert product to database
     * @param array $product
     */
    protected function insertProduct($product)
    {
        // get database name
        $dataBaseName   = $this->getDocumentManager()
            ->getConfiguration()
            ->getDefaultDB();
        // get collection name
        $collectionName = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getClassMetadata()
            ->collection;

        // insert new document
        $this->getDocumentManager()->getConnection()
            ->selectDatabase($dataBaseName)
            ->selectCollection($collectionName)
            ->insert($product);

        // find inserted document
        $hydratedProduct = $this->getDocumentManager()
            ->find('NitraProductBundle:Product', (string) $product['_id']);

        // set aliases to null for regenerate
        $hydratedProduct->setAliasEn(null);
        $hydratedProduct->setAliasRu(null);
        // set name
        $hydratedProduct->setName('Копия - "' . $hydratedProduct . '" (' . date("Y-m-d H:m:s") . ')');

        // flush
        $this->getDocumentManager()->flush($hydratedProduct);
    }
}
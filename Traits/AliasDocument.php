<?php

namespace Nitra\ProductBundle\Traits;

use Nitra\ProductBundle\Sluggable\Mapping\Annotation\Slug;

trait AliasDocument
{
    /**
     * Алиас en для ссылки
     * @Slug(fields={"name"})
     * @ODM\String
     */
    protected $aliasEn;

    /**
     * Алиас ru для ссылки
     * @Slug(
     *      fields={"name"},
     *      transliterator={"\Nitra\ProductBundle\Sluggable\Transliterators\Russian", "transliterate"},
     *      urlizer={"\Nitra\ProductBundle\Sluggable\Transliterators\Russian", "urlize"}
     * )
     * @ODM\String
     */
    protected $aliasRu;

    /**
     * Set aliasEn
     * @param string $aliasEn
     * @return self
     */
    public function setAliasEn($aliasEn)
    {
        $this->aliasEn = $aliasEn;
        return $this;
    }

    /**
     * Get aliasEn
     * @return string $aliasEn
     */
    public function getAliasEn()
    {
        return $this->aliasEn;
    }

    /**
     * Set aliasRu
     * @param string $aliasRu
     * @return self
     */
    public function setAliasRu($aliasRu)
    {
        $this->aliasRu = $aliasRu;
        return $this;
    }

    /**
     * Get aliasRu
     * @return string $aliasRu
     */
    public function getAliasRu()
    {
        return $this->aliasRu;
    }
}
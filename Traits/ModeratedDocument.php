<?php

namespace Nitra\ProductBundle\Traits;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Trait for use moderated options
 */
trait ModeratedDocument
{
    /**
     * @var string
     * @ODM\String
     * @Gedmo\Blameable(on="change", field={"isModerated"})
     */
    protected $moderatedBy;

    /**
     * @var \DateTime
     * @ODM\Date
     * @Gedmo\Timestampable(on="change", field={"isModerated"})
     */
    protected $moderatedAt;

    /**
     * @var boolean
     * @ODM\Boolean
     */
    protected $isModerated;

    /**
     * Set moderatedBy.
     * @param string $moderatedBy
     * @return self
     */
    public function setModeratedBy($moderatedBy)
    {
        $this->moderatedBy = $moderatedBy;
        return $this;
    }

    /**
     * Get moderatedBy.
     * @return string
     */
    public function getModeratedBy()
    {
        return $this->moderatedBy;
    }

    /**
     * Set moderatedAt
     * @param \DateTime $moderatedAt
     * @return self
     */
    public function setModeratedAt($moderatedAt)
    {
        $this->moderatedAt = $moderatedAt;
        return $this;
    }

    /**
     * Get moderatedAt
     * @return \DateTime
     */
    public function getModeratedAt()
    {
        return $this->moderatedAt;
    }

    /**
     * Set is moderatede
     * @param int $isModerated
     * @return self
     */
    public function setIsModerated($isModerated)
    {
        $this->isModerated = $isModerated;
        return $this;
    }

    /**
     * Get isModerated
     * @return int $isModerated
     */
    public function getIsModerated()
    {
        return $this->isModerated;
    }
}
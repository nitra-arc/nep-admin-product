<?php

namespace Nitra\ProductBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Nitra\ProductBundle\Document\Product;

class SearchListener
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    protected $config;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\Container $container, array $config)
    {
        $this->container = $container;
        $this->config    = $config;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Event\OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        ini_set('memory_limit', '-1');
        $dm      = $args->getDocumentManager();
        $uow     = $dm->getUnitOfWork();
        $entries = array_merge(
            $uow->getScheduledDocumentUpdates(), $uow->getScheduledDocumentInsertions()
        );

        foreach ($entries as $entry) {
            $this->generateSearchField($dm, $entry);
        }
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Nitra\ProductBundle\Document\Product $doc
     */
    protected function generateSearchField(\Doctrine\ODM\MongoDB\DocumentManager $dm, $doc)
    {
        $products = $this->isInstance($dm, $doc);
        if ($products && count($products)) {
            $productToCheckChanges = end($products);
            if ($productToCheckChanges->getFullNameForSearch() == $this->generate($productToCheckChanges)) {
                return;
            }
            foreach ($products as $product) {
                $product->setFullNameForSearch($this->generate($product));
                $uow  = $dm->getUnitOfWork();
                $meta = $dm->getClassMetadata(get_class($product));
                $uow->recomputeSingleDocumentChangeSet($meta, $product);
            }
        }
    }

    /**
     * @param Product $doc
     * @return string
     */
    protected function generate($doc)
    {
        $words = array();
        foreach ($this->config['fields'] as $field) {
            if ($val = $this->getValue($doc, $field)) {
                $words[] = $val;
            }
        }

        return trim(implode(' ', $words));
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Nitra\ProductBundle\Document\Product $doc
     * @return \Nitra\ProductBundle\Document\Product[]|boolean
     */
    protected function isInstance($dm, $doc)
    {
        if ($doc instanceof Product) {
            return array($doc);
        } else {
            foreach ($this->config['instanceof'] as $field => $instance) {
                if ($doc instanceof $instance) {
                    return $this->getProductsBy($dm, array(
                        $field . '.$id' => $this->toMongoId($doc->getId()),
                    ));
                }
            }
        }

        return false;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param array $parameters
     * @return \Doctrine\MongoDB\Cursor
     */
    protected function getProductsBy(\Doctrine\ODM\MongoDB\DocumentManager $dm, array $parameters)
    {
        return $dm->getRepository('NitraProductBundle:Product')
            ->findBy($parameters);
            //->toArray();
    }

    /**
     * @param \MongoId|string $id
     * @return \MongoId
     */
    protected function toMongoId($id)
    {
        return ($id instanceof \MongoId) ? $id : new \MongoId($id);
    }

    /**
     * Получение значения из товара
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param string $field
     * @return string
     */
    protected function getValue($product, $field)
    {
        $val = null;
        if (is_array($field)) {
            $obj = $product;
            foreach ($field as $method) {
                if (is_object($obj) && method_exists($obj, $method)) {
                    $obj = $obj->$method();
                }
            }
            if (is_object($obj) || is_string($obj)) {
                $val = (string) $obj;
            }
        } elseif (method_exists($product, $field)) {
            $val = $product->$field();
        }

        return (string) $val;
    }
}
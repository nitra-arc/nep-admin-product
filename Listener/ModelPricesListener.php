<?php

namespace Nitra\ProductBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ModelPricesListener
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface   Container instance
     */
    protected $container;

    /**
     * @var string Current store identifier
     */
    protected $storeId;

    /**
     * Construct
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container Container instance
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get store identifier
     *
     * @return string|null Identifier of current store
     */
    protected function getStoreId()
    {
        if ($this->storeId) {
            return $this->storeId;
        }

        if (!$this->container->isScopeActive('request')) {
            return null;
        }

        $this->storeId = $this->container->get('session')->get('store_id');
        return $this->storeId;
    }

    /**
     * On flush doctrine event handler
     *
     * @param \Doctrine\ODM\MongoDB\Event\OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $dm      = $args->getDocumentManager();
        $uow     = $dm->getUnitOfWork();
        $entries = array_merge(
            $uow->getScheduledDocumentUpdates(),
            $uow->getScheduledDocumentInsertions()
        );

        foreach ($entries as $entry) {
            if ($entry instanceof \Nitra\ProductBundle\Document\Product) {
                $this->processProduct($entry, $dm);
            } elseif ($entry instanceof \Nitra\ProductBundle\Document\Model) {
                $this->processModel($entry, $dm);
            }
        }
    }

    /**
     * Process product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    protected function processProduct($product, $dm)
    {
        $modelPrices = $this->getModelPrices($product->getModel());

        $needCompute = $this->updateModelPricesByProduct($product, $modelPrices);

        if ($needCompute) {
            $product->getModel()->setPrices($modelPrices);
            $meta = $dm->getRepository('NitraProductBundle:Model')->getClassMetadata();
            $dm->getUnitOfWork()->computeChangeSet($meta, $product->getModel());
        }
    }

    /**
     * Process model
     *
     * @param \Nitra\ProductBundle\Document\Model   $model
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    protected function processModel($model, $dm)
    {
        $modelPrices = $this->getModelPrices($model);

        $needCompute = false;
        foreach ($model->getProducts() as $product) {
            if ($this->updateModelPricesByProduct($product, $modelPrices)) {
                $needCompute = true;
            }
        }

        if ($needCompute) {
            $model->setPrices($modelPrices);
            $meta = $dm->getRepository('NitraProductBundle:Model')->getClassMetadata();
            $dm->getUnitOfWork()->computeChangeSet($meta, $model);
        }
    }

    /**
     * Update model prices by product price
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param array                                 $modelPrices
     *
     * @return boolean  Need compute changes
     */
    protected function updateModelPricesByProduct($product, &$modelPrices)
    {
        $price       = $this->getProductPrice($product);
        $needCompute = false;
        if (is_null($modelPrices[$this->getStoreId()]['from']) || ($price < $modelPrices[$this->getStoreId()]['from'])) {
            $modelPrices[$this->getStoreId()]['from'] = $price;
            $needCompute = true;
        }
        if (is_null($modelPrices[$this->getStoreId()]['to']) || ($price > $modelPrices[$this->getStoreId()]['to'])) {
            $modelPrices[$this->getStoreId()]['to'] = $price;
            $needCompute = true;
        }

        return $needCompute;
    }

    /**
     * Get prices from model
     *
     * @param \Nitra\ProductBundle\Document\Model $model
     *
     * @return array
     */
    protected function getModelPrices($model)
    {
        $prices = $model->getPrices();

        if (!array_key_exists($this->getStoreId(), $prices)) {
            $prices[$this->getStoreId()] = array(
                'from'  => null,
                'to'    => null,
            );
        }

        return $prices;
    }

    /**
     * Get price of current store for product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return float|null
     */
    protected function getProductPrice($product)
    {
        $storePrices = $product->getStorePrice();
        $storePrice  = array_key_exists($this->getStoreId(), $storePrices)
            ? $storePrices[$this->getStoreId()]
            : array();

        return array_key_exists('price', $storePrice)
            ? (float) $storePrice['price']
            : null;
    }
}
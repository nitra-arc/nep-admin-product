<?php

namespace Nitra\ProductBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Nitra\ProductBundle\Document\Category;
use Nitra\ProductBundle\Document\Product;

class AliasListener
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /**
     * Constructor
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * On flush doctrine event handler
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $dm      = $args->getDocumentManager();
        $uow     = $dm->getUnitOfWork();

        $entries = array_merge(
            $uow->getScheduledDocumentUpdates(),
            $uow->getScheduledDocumentInsertions()
        );

        foreach ($entries as $entrie) {
            if ($entrie instanceof Product) {
                //$entrie->setFullUrlAliasRu($this->generateProductAliasRu($entrie));
                if ($this->checkNeedleToGenerateFullUrlAlias($entrie, $uow)) {
                    $entrie->setFullUrlAliasEn($this->generateProductAliasEn($entrie));
                }
            } elseif ($entrie instanceof Category) {
                //$entrie->setFullUrlAliasRu($this->generateCategoryAliasRu($entrie));
                if ($this->checkNeedleToGenerateFullUrlAlias($entrie, $uow)) {
                    $entrie->setFullUrlAliasEn($this->generateCategoryAliasEn($entrie));

                    $this->regenerateChildrens($dm, $entrie);
                }
            }
            $meta = $dm->getClassMetadata(get_class($entrie));
            $uow->recomputeSingleDocumentChangeSet($meta, $entrie);
        }
    }

    /**
     * Check of need to regenerate full url alias
     * @param object                            $object
     * @param \Doctrine\ODM\MongoDB\UnitOfWork  $uow
     * @return boolean
     */
    protected function checkNeedleToGenerateFullUrlAlias($object, $uow)
    {
        $changeSets = $uow->getDocumentChangeSet($object);

        return array_key_exists('aliasEn', $changeSets) || array_key_exists('fullUrlAliasEn', $changeSets);
    }

    /**
     * Перерегенирация дочерних товаров и категорий
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param Category $category
     */
    protected function regenerateChildrens($dm, $category)
    {
        $uow       = $dm->getUnitOfWork();
        $modelsIds = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('_id')
            ->field('category.id')->equals($category->getId())
            ->getQuery()->execute()->toArray();

        $products = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in(array_keys($modelsIds))
            ->getQuery()->execute();

        foreach ($products as $product) {
            //$product->setFullUrlAliasRu($this->generateProductAliasRu($product));
            $product->setFullUrlAliasEn($this->generateProductAliasEn($product));

            $meta = $dm->getClassMetadata(get_class($product));
            $uow->computeChangeSet($meta, $product);
        }

        $categories = $dm->createQueryBuilder('NitraProductBundle:Category')
            ->field('parent.id')->equals($category->getId())
            ->getQuery()->execute();

        foreach ($categories as $child) {
            //$child->setFullUrlAliasRu($this->generateCategoryAliasRu($child));
            $child->setFullUrlAliasEn($this->generateCategoryAliasEn($child));

            $meta = $dm->getClassMetadata(get_class($child));
            $uow->computeChangeSet($meta, $child);

            $this->regenerateChildrens($dm, $child);
        }
    }

    protected function generateProductAliasEn($product)
    {
        $slugs = array();
        $slugs[$product->getId()] = $product->getAliasEn();
        $slugs[$product->getModel()->getId()] = $product->getModel()->getAliasEn();
        $slugs += $this->getParentsCategoriesEn($product->getModel()->getCategory());

        return implode('/', array_reverse($slugs));
    }

    protected function generateProductAliasRu($product)
    {
        $slugs = array();
        $slugs[$product->getId()] = $product->getAliasRu();
        $slugs[$product->getModel()->getId()] = $product->getModel()->getAliasRu();
        $slugs += $this->getParentsCategoriesRu($product->getModel()->getCategory());

        return implode('/', array_reverse($slugs));
    }

    protected function generateCategoryAliasEn($category)
    {
        $slugs = $this->getParentsCategoriesEn($category);

        return implode('/', array_reverse($slugs));
    }

    protected function generateCategoryAliasRu($category)
    {
        $slugs = $this->getParentsCategoriesRu($category);

        return implode('/', array_reverse($slugs));
    }

    protected function getParentsCategoriesEn($category, $slugs = array())
    {
        $slugs[count($slugs) + 1] = $category->getAliasEn();
        if ($category->getParent()) {
            $slugs += $this->getParentsCategoriesEn($category->getParent(), $slugs);
        }

        return $slugs;
    }

    protected function getParentsCategoriesRu($category, $slugs = array())
    {
        $slugs[count($slugs) + 1] = $category->getAliasRu();
        if ($category->getParent()) {
            $slugs += $this->getParentsCategoriesRu($category->getParent(), $slugs);
        }

        return $slugs;
    }
}
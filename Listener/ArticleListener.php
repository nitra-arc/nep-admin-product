<?php

namespace Nitra\ProductBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Nitra\ProductBundle\Document\Product;

class ArticleListener
{
    /**
     * On flush doctrine event handler
     *
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        // get document manager
        $dm   = $args->getDocumentManager();
        // get unit of work
        $uow  = $dm->getUnitOfWork();
        // get class metadata for product
        $meta = $dm->getRepository('NitraProductBundle:Product')->getClassMetadata();

        // get insertions documents
        $insertions = $uow->getScheduledDocumentInsertions();

        // iterate insertions
        foreach ($insertions as $insertion) {
            // if insertion is not product
            if (!$insertion instanceof Product) {
                continue;
            }

            // generate counter and article (if not defined)
            $this->generateCounter($insertion, $dm);

            // recompute changes
            $uow->recomputeSingleDocumentChangeSet($meta, $insertion);
        }
    }

    /**
     * Generate product counter
     *
     * @param Product $product
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    protected function generateCounter($product, $dm)
    {
        // get product with maximal counter
        $productWithMaxCounter = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->sort('counter', -1)
            ->getQuery()->execute()->getSingleResult();

        // calculate new counter
        $counter = $productWithMaxCounter
            // if has product with max counter - get him and add 1
            ? ($productWithMaxCounter->getCounter() + 1)
            : 100000;

        // set counter
        $product->setCounter($counter);

        // if article for product is not defined
        if (is_null($product->getArticle())) {
            // set article
            $product->setArticle($counter);
        }
    }
}
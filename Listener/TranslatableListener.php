<?php

namespace Nitra\ProductBundle\Listener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class TranslatableListener
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \Gedmo\Translatable\TranslatableListener */
    protected $translatableListener;

    protected $skipPattern = '/^.*\/edit$|^.*\/new$|^.*\/update$/';

    public function __construct(Container $container)
    {
        $this->container            = $container;
        $this->translatableListener = $container->get('stof_doctrine_extensions.listener.translatable');
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (preg_match($this->skipPattern, $request->getRequestUri())) {
            $this->translatableListener
                ->setSkipOnLoad(true);
        }
    }
}
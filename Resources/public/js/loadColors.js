(function( $ ) {
    $.fn.loadColors = function(options) {
        var settings = $.extend({
            path        : null,        // '{{path("Nitra_ProductBundle_loadColors") }}'
            replaceId   : '#prod_color',
            coloredTag  : '<div id="prod_color" style="width: 50px; height: 50px; background-color: #%color%"></div>',
            imagedTag   : '<img id="prod_color" src="%image%" width="50" height="50">',
            load        : function (item, settings) {
                var val = $(item).val();
                var ids = [val];

                $.ajax({
                    url:        settings.path,
                    type:       'POST',
                    data:       {
                        colors_ids: ids
                    },
                    success:    function(colors) {
                        $(settings.replaceId).remove();
                        for (var i in colors){
                            var tag = (colors[i].image === null)
                                ? settings.coloredTag.replace(/%color%/, colors[i].color)
                                : settings.imagedTag.replace(/%image%/, colors[i].image);
                            $(item).before(tag);
                        }
                    }
                });
            }
        }, options); 
        
        return this.each(function() {
            $(this).on('change', function() {
                settings.load(this, settings);
            });
            settings.load(this, settings);
        });
    };
})(jQuery);
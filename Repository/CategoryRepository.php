<?php

namespace Nitra\ProductBundle\Repository;

use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;

class CategoryRepository extends MaterializedPathRepository
{
    /**
     * Получение id всех дочерних категоирй с текущей
     * @param \Nitra\ProductBundle\Document\Category    $category
     * @return array
     */
    public function getChildrenTreeId($category)
    {
        $children = $this->getChildrenQueryBuilder($category)
            ->hydrate(false)
            ->getQuery()
            ->execute()
            ->toArray();

        $children[$category->getId()] = $category->getId();

        return array_keys($children);
    }

    /**
     * Получение id всех родительских категоирй с текущей
     * @param \Nitra\ProductBundle\Document\Category    $category
     * @param array                                     $treeId
     * @return array
     */
    public function getParentTreeId($category, $treeId = array())
    {
        $treeId[] = $category->getId();
        if ($category->getParent()) {
            return $this->getParentTreeId($category->getParent(), $treeId);
        }

        return $treeId;
    }
}
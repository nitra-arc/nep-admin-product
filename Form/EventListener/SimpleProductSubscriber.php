<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SimpleProductSubscriber implements EventSubscriberInterface
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    /** @var \Symfony\Component\Translation\TranslatorInterface */
    protected $translator;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
//    public function __construct(ContainerInterface $container)
//    {
//        $this->container    = $container;
//        $this->dm           = $container->get('doctrine_mongodb.odm.document_manager');
//        $this->translator   = $container->get('translator');
//    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA   => 'postSetData',
            FormEvents::POST_SUBMIT     => 'postSubmit',
        );
    }

    /**
     * Getter for store id
     * @return string
     */
//    protected function getStoreId()
//    {
//        return $this->container->isScopeActive('request')
//            ? $this->container->get('session')->get('store_id')
//            : null;
//    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function postSetData(FormEvent $event)
    {
        $product = $event->getData();
        $form    = $event->getForm();

        $form->get('model')->setData((string) $product->getModel());
    }

    /**
     * Post submit form handler
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
//        if (!$event->getForm()->isValid()) {
//            return;
//        }
//
//        $this->setStorePrice($event->getData(), $event->getForm());
    }
}
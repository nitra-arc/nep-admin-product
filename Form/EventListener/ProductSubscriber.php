<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Nitra\ProductBundle\Form\EventListener\AbstractProductModelParametersSubscriber;
use Symfony\Component\Form\FormEvent;

class ProductSubscriber extends AbstractProductModelParametersSubscriber
{
    /** @var boolean */
    protected $simple = false;

    /**
     * @param boolean $simple
     * @return self
     */
    public function setIsSimple($simple)
    {
        $this->simple = $simple;
        return $this;
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $product = $event->getData();
        $form    = $event->getForm();

        if (!key_exists('NitraProductSeparationBundle', $this->container->getParameter('kernel.bundles')) && $form->has('status')) {
            $form->remove('status');
        }

        if (!$this->simple) {
            $this->removeCommonModelFields($form);
        } else {
            $this->formModel($form, $product);
            $this->formCategory($form, $product);
            $this->formBrand($form, $product);
            $this->formStores($form, $product);
        }

        if (!$product) {
            $class = $this->dm->getRepository('NitraProductBundle:Product')->getClassName();
            $event->setData(new $class);
            $form->remove('parameters');
            return;
        }

        $this->formPrice($form, $product);
        $this->formDiscount($form, $product);
        $this->formFixedPrice($form, $product);
    }

    /**
     * Post submit form handler
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $form    = $event->getForm();
        $product = $event->getData();

        if (!$form->isValid()) {
            return;
        }

        $postData = $this->getPostDataFromRequest($form);

        $this->setStorePrice($product, $postData);
        if ($this->simple) {
            $this->setModel($product, $postData);
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formPrice($form, $product)
    {
        if ($form->has('curentStorePrice')) {
            $options    = $form->get('curentStorePrice')->getConfig()->getOptions();

            $storePrice = $product->getStorePrice();
            $value      = key_exists($this->getStoreId(), $storePrice) && key_exists('price', $storePrice[$this->getStoreId()])
                ? $storePrice[$this->getStoreId()]['price']
                : null;

            if (!key_exists('attr', $options)) {
                $options['attr'] = array();
            }
            $options['attr']['value'] = $value;

            $form->add('curentStorePrice', 'number', $options);
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formDiscount($form, $product)
    {
        if ($form->has('curentStoreDiscount')) {
            $options    = $form->get('curentStoreDiscount')->getConfig()->getOptions();

            $storePrice = $product->getStorePrice();
            $value      = key_exists($this->getStoreId(), $storePrice) && key_exists('discount', $storePrice[$this->getStoreId()])
                ? $storePrice[$this->getStoreId()]['discount']
                : null;

            if (!key_exists('attr', $options)) {
                $options['attr'] = array();
            }
            $options['attr']['value'] = $value;

            $form->add('curentStoreDiscount', 'number', $options);
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formModel($form, $product)
    {
        if ($form->has('model')) {
            $options      = $form->get('model')->getConfig()->getOptions();
            $modelOptions = array(
                'label'              => $options['label'],
                'help'               => $options['help'],
                'translation_domain' => $options['translation_domain'],
                'mapped'             => false,
                'class'              => 'Nitra\ProductBundle\Document\Model',
                'data'               => $product->getModel()->getName(),
                'configs'            => array(
                    'limit'     => 10,
                    'minLength' => 1,
                ),
            );

            $form->add('model', 'genemu_jqueryautocomplete_document', $modelOptions);
        }
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formCategory($form, $product)
    {
        $form->add('category', 'nl_tree_category', array(
            'help'               => 'fields.category.help',
            'label'              => 'fields.category.label',
            'translation_domain' => 'NitraProductBundleModel',
            'required'           => true,
            'mapped'             => false,
            'data'               => $product->getModel()->getCategory()->getId(),
        ));
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formBrand($form, $product)
    {
        $form->add('brand', 'brand_filter', array(
            'class'              => 'Nitra\\ProductBundle\\Document\\Brand',
            'help'               => 'fields.brand.help',
            'label'              => 'fields.brand.label',
            'multiple'           => false,
            'translation_domain' => 'NitraProductBundleModel',
            'mapped'             => false,
            'data'               => $product->getModel()->getBrand()->getId(),
        ));
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formStores($form, $product)
    {
        $form->add('stores', 'document', array(
            'class'              => 'Nitra\\StoreBundle\\Document\\Store',
            'multiple'           => true,
            'help'               => 'fields.stores.help',
            'expanded'           => true,
            'required'           => false,
            'label'              => 'fields.stores.label',
            'translation_domain' => 'NitraProductBundleModel',
            'mapped'             => false,
            'data'               => $product->getModel()->getStores(),
        ));
    }

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function formFixedPrice($form, $product)
    {
        if ($form->has('isFixedPrice')) {
            $options = $form->get('isFixedPrice')->getConfig()->getOptions();

            $storePrice = $product->getStorePrice();
            $value = key_exists($this->getStoreId(), $storePrice) && key_exists('isFixedPrice', $storePrice[$this->getStoreId()])
                ? $storePrice[$this->getStoreId()]['isFixedPrice']
                : false;

            if (!key_exists('attr', $options)) {
                $options['attr'] = array();
            }
            if ($value) {
                $options['attr']['checked'] = 'checked';
            }

            $form->add('isFixedPrice', 'checkbox', $options);
        }
    }

    /**
     * Сохранение цены для текущего магазина
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param array $postData
     */
    protected function setStorePrice($product, $postData)
    {
        $storePrices = $product->getStorePrice();

        if (!is_array($storePrices)) {
            $storePrices = array();
        }

        if (!array_key_exists($this->getStoreId(), $storePrices)) {
            $storePrices[$this->getStoreId()] = array();
        }

        $storePrice = &$storePrices[$this->getStoreId()];

        if (!array_key_exists('isFixedPrice', $postData)) {
            $postData['isFixedPrice'] = false;
        }

        $storePrice['price']        = (float) $postData['curentStorePrice'];
        $storePrice['discount']     = (float) $postData['curentStoreDiscount'];
        $storePrice['isFixedPrice'] = (boolean) $postData['isFixedPrice'];

        $product->setStorePrice($storePrices);
    }

    /**
     * Сохранение модели товара
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param array $postData
     */
    protected function setModel($product, $postData)
    {
        $name       = $postData['model'];
        $categoryId = $postData['category'];
        $brandId    = $postData['brand'];
        $storesId   = $postData['stores'];

        $model = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->field('name')->equals($name)
            ->field('category.id')->equals($categoryId)
            ->field('brand.id')->equals($brandId)
            ->field('stores.id')->in($storesId)
            ->getQuery()->execute()->getSingleResult();

        if (!$model) {
            $model = new \Nitra\ProductBundle\Document\Model();

            $category = $this->dm->find('NitraProductBundle:Category', $categoryId);
            $brand    = $this->dm->find('NitraProductBundle:Brand', $brandId);
            $stores   = $this->dm->createQueryBuilder('NitraStoreBundle:Store')
                ->field('id')->in($storesId)
                ->getQuery()->execute();

            $model->setName($name);
            $model->setCategory($category);
            $model->setBrand($brand);
            foreach ($stores as $store) {
                $model->addStore($store);
            }

            $model->addProduct($product);

            $this->dm->persist($model);
        }
    }

    /**
     * Remove common fields for model products
     * @param \Symfony\Component\Form\FormInterface $form
     */
    protected function removeCommonModelFields($form)
    {
        $store = $this->dm->find('NitraStoreBundle:Store', $this->getStoreId());

        foreach ($store->getProductManagement()->getCommonProductsFields() as $field) {
            if ($form->has($field)) {
                $form->remove($field);
            }
        }
    }
}
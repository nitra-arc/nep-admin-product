<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Nitra\ProductBundle\Form\EventListener\AbstractProductModelParametersSubscriber;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Yaml\Yaml;

class ModelSubscriber extends AbstractProductModelParametersSubscriber
{
    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        $form->add('commonProductsFields', 'hidden', array(
            'data'   => $this->addCommonProductsFields($form, $event->getData()),
            'mapped' => false,
        ));
    }

    /**
     * Post submit form handler
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $form  = $event->getForm();
        $model = $event->getData();
        if (!$form->isValid()) {
            return;
        }

        foreach ($model->getProducts() as $product) {
            if (!$product->getId()) {
                $this->dm->persist($product);
                $this->dm->flush($product);
            }
        }

        $this->removeProducts($model);

        if (($updated = $this->updateProducts($event)) && ($request = $this->getRequest())) {
            $request->getSession()->getFlashBag()->add(
                'success',
                $this->translator->trans('actions.products.successUpdated', array(
                    '%amount%' => $updated,
                ), 'NitraProductBundleModel')
            );
        }
    }

    /**
     * Get common model products fields from store settings
     * @return array
     */
    protected function getCommonFields()
    {
        $store = $this->dm->find('NitraStoreBundle:Store', $this->getStoreId());

        return $store->getProductManagement()->getCommonProductsFields();
    }

    /**
     * Add common model products fields to form
     * @param \Symfony\Component\Form\FormInterface $form
     * @param \Nitra\ProductBundle\Document\Model   $model
     * @return array
     */
    protected function addCommonProductsFields($form, $model)
    {
        $commonFields = $this->getCommonFields();
        $fields       = $this->getFieldsFromYaml();

        $product      = $this->dm->getRepository('NitraProductBundle:Product')->findOneBy(array(
            'model.$id'   => new \MongoId($model->getId()),
        ));

        foreach ($commonFields as $field) {
            list ($type, $options) = $this->formatFieldOptionsParameters($field, $fields, $product);
            $form->add('common_products_field_' . $field, $type, $options);
        }

        return $commonFields;
    }

    /**
     * Get fields for products from Product-generator.yml
     * @return array
     */
    protected function getFieldsFromYaml()
    {
        $generator = $this->getProductGeneratorPath();

        $parsed    = $this->container->get('admingenerator.parser.yaml')->parse($generator);

        return $parsed['params']['fields'];
    }

    /**
     * Get path to Product-generator.yml
     * @return string Path to Product-generator.yml file
     */
    protected function getProductGeneratorPath()
    {
        $bundlePath = $this->kernel->getBundle('NitraProductBundle')->getPath();

        return $bundlePath .
            DIRECTORY_SEPARATOR .
            'Resources' .
            DIRECTORY_SEPARATOR .
            'config' .
            DIRECTORY_SEPARATOR .
            'Product-generator.yml'
        ;
    }

    /**
     * Format options for field
     * @param string                                        $field              Field name
     * @param array                                         $generatorFields    Parsed fields from generator
     * @param \Nitra\ProductBundle\Document\Product|null    $product            Product of model
     * @return array
     */
    protected function formatFieldOptionsParameters($field, $generatorFields, $product)
    {
        $options = array(
            'translation_domain' => 'NitraProductBundleProduct',
            'label'              => $generatorFields[$field]['label'],
            'mapped'             => false,
        );
        list ($dataField, $dataValue) = $this->getFieldData($field, $product);

        $options[$dataField] = $dataValue;

        $type = $this->getFormType($field, $generatorFields[$field]);

        if (array_key_exists('addFormOptions', $generatorFields[$field])) {
            $options += $generatorFields[$field]['addFormOptions'];
            $options['virtual'] = false;
        }

        if ($class = $this->getFieldClass($field, $type)) {
            $options['class'] = $class;
        }

        return array(
            preg_match('/\\\\/', $type) ? new $type : $type,
            $options,
        );
    }

    /**
     * Get data from product by field
     * @param string                                        $field      Field name
     * @param \Nitra\ProductBundle\Document\Product|null    $product    Product of model
     * @return array
     */
    protected function getFieldData($field, $product)
    {
        $dataField = 'data';
        $dataValue = null;

        if ($product) {
            $cm = $this->dm->getClassMetadata(get_class($product));

            if ($cm->hasField($field)) {
                $dataValue = $cm->getReflectionProperty($field)->getValue($product);
            }

            $method = 'getProductData' . ucfirst($field);

            if (method_exists($this, $method)) {
                return $this->$method($product);
            }
        }

        return array(
            $dataField,
            $dataValue,
        );
    }

    /**
     * Get store price data from product
     * @param string $key
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return array Current store price from product
     */
    protected function getProductDataStorePrice($key, $product)
    {
        $value = null;
        if (array_key_exists($this->getStoreId(), $product->getStorePrice()) &&
            array_key_exists($key, $product->getStorePrice()[$this->getStoreId()])
        ) {
            $value = $product->getStorePrice()[$this->getStoreId()][$key];
        }

        return array(
            'attr',
            array(
                'value' => $value,
            ),
        );
    }

    /**
     * Get store price data <b>price</b> from product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return array Current store price data from product
     */
    protected function getProductDataCurentStorePrice($product)
    {
        return $this->getProductDataStorePrice('price', $product);
    }

    /**
     * Get store price data <b>discount</b> from product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return array Current store price discount from product
     */
    protected function getProductDataCurentStoreDiscount($product)
    {
        return $this->getProductDataStorePrice('discount', $product);
    }

    /**
     * Get store price data <b>isFixedPrice</b> from product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return array Current store price isFixedPrice from product
     */
    protected function getProductDataIsFixedPrice($product)
    {
        $attr = $this->getProductDataStorePrice('isFixedPrice', $product);
        $checked = $attr[1]['value'];

        $attr[1]['checked'] = $checked ? 'checked' : null;

        return $attr;
    }

    /**
     * Get form type of field
     * @param string $field
     * @param array  $parameters
     * @return string
     * @throws \Admingenerator\GeneratorBundle\Exception\NotImplementedException
     */
    protected function getFormType($field, $parameters)
    {
        if (array_key_exists('formType', $parameters)) {
            return $parameters['formType'];
        }

        $dbType = $this->getDbType($field);

        $formTypes = $this->container->getParameter('admingenerator.doctrineodm_form_types');

        if ($dbType == 'date') {
            return 'date';
        } elseif (array_key_exists($dbType, $formTypes)) {
            return $formTypes[$dbType];
        } else {
            throw new \Admingenerator\GeneratorBundle\Exception\NotImplementedException(
                'The dbType "' .
                $dbType .
                '" is not yet implemented ' .
                '(field "' .
                $field .
                '")'
            );
        }
    }

    /**
     * Get data base type of field
     * @param string $field
     * @return string
     */
    protected function getDbType($field)
    {
        $metadata = $this->dm->getRepository('NitraProductBundle:Product')->getClassMetadata();

        if ($metadata->hasAssociation($field)) {
            return $metadata->isSingleValuedAssociation($field)
                ? 'document'
                : 'collection';
        }

        if ($metadata->hasField($field)) {
            $mapping = $metadata->getFieldMapping($field);

            return $mapping['type'];
        }

        return 'virtual';
    }

    /**
     * Get class option for field
     * @param string $field
     * @param string|object $type
     * @return string
     */
    protected function getFieldClass($field, $type)
    {
        if (is_object($type)) {
            return;
        }

        if (preg_match("#^(document|accessories)#i", $type) || preg_match("#(document|accessories)$#i", $type)) {
            $metadata = $this->dm->getRepository('NitraProductBundle:Product')->getClassMetadata();
            $mapping = $metadata->getFieldMapping($field);

            return $mapping['targetDocument'];
        }

        return;
    }

    /**
     * Remove deleted products
     * @param \Nitra\ProductBundle\Document\Model $model
     */
    protected function removeProducts($model)
    {
        $existsMongoIds = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('id')
            ->field('model.id')->equals($model->getId())
            ->getQuery()->execute()->toArray();

        foreach ($model->getProducts() as $product) {
            if (array_key_exists($product->getId(), $existsMongoIds)) {
                unset ($existsMongoIds[$product->getId()]);
            }
        }

        if ($existsMongoIds) {
            $products = $this->dm->createQueryBuilder('NitraProductBundle:Product')
                ->field('id')->in(array_keys($existsMongoIds))
                ->getQuery()->execute();

            foreach ($products as $product) {
                $this->dm->remove($product);
            }
        }
    }

    /**
     * Update products of model
     * @param FormEvent $event
     * @return int|boolean <b>integer</b> - Count of updated products<br>
     * <b>boolean</b> - false If common fields not exists
     */
    protected function updateProducts($event)
    {
        $model        = $event->getData();
        $form         = $event->getForm();
        $commonFields = $this->getCommonFields();

        if (!$commonFields) {
            return false;
        }

        $qb = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->update()->multiple()
            ->field('model.id')->equals($model->getId());

        foreach ($commonFields as $field) {
            $data = $form->get('common_products_field_' . $field)->getData();
            $method = 'convert' . ucfirst($field) . 'ToDatabaseValue';

            if (method_exists($this->productConverter, $method)) {
                list ($dbField, $dbValue) = $this->productConverter->$method($data);
                $qb->field($dbField)->set($dbValue);
                continue;
            }

            $qb->field($field)->set($data);
        }

        return $qb->getQuery()->execute()['n'];
    }
}
<?php

namespace Nitra\ProductBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormEvents;

abstract class AbstractProductModelParametersSubscriber implements EventSubscriberInterface
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    /** @var \Symfony\Component\HttpKernel\KernelInterface */
    protected $kernel;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    /** @var \Symfony\Component\Translation\TranslatorInterface */
    protected $translator;
    /** @var \Nitra\ProductBundle\Lib\ProductToMongoDBConverter */
    protected $productConverter;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container        = $container;
        $this->dm               = $container->get('doctrine_mongodb.odm.document_manager');
        $this->translator       = $container->get('translator');
        $this->kernel           = $container->get('kernel');
        $this->productConverter = $container->get('nitra.product.converter_to_database_value');
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA    => 'preSetData',
            FormEvents::POST_SUBMIT     => 'postSubmit',
        );
    }

    /**
     * Getter for store id
     * @return string
     */
    protected function getStoreId()
    {
        return $this->container->isScopeActive('request')
            ? $this->container->get('session')->get('store_id')
            : null;
    }

    /**
     * Getter for request object
     * @return \Symfony\Component\HttpFoundation\Request
     */
    protected function getRequest()
    {
        return $this->container->isScopeActive('request')
            ? $this->container->get('request')
            : null;
    }

    /**
     * Get post data from request by form name
     * @param \Symfony\Component\Form\FormInterface $form
     * @return array
     */
    protected function getPostDataFromRequest($form)
    {
        $fields = array_reverse($this->getFormPath($form));

        $postData = $this->container->get('request')->request->all();

        $result = $postData;
        foreach ($fields as $field) {
            $result = $result[$field];
        }

        return $result;
    }

    /**
     * Reqoursive getting path to form data
     * @param \Symfony\Component\Form\FormInterface $form
     * @param array $fields
     */
    protected function getFormPath($form, $fields = array())
    {
        $fields[] = $form->getName();

        return $form->getParent()
            ? $this->getFormPath($form->getParent(), $fields)
            : $fields;
    }
}
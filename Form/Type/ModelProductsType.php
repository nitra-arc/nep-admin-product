<?php

namespace Nitra\ProductBundle\Form\Type;

use Admingenerated\NitraProductBundle\Form\BaseProductType\EditType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ModelProductsType extends EditType
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        foreach ($builder->get('translations')->getOptions()['fields'] as $key => $options) {
            $builder->add($key, 'nl_translation', array(
                'required'           => false,
                'fields'             => array(
                    $key    => $options,
                ),
                'label'              => ' ',
            ));
        }

        $builder->addEventSubscriber($this->container->get('nitra.parameters.subscriber'));
        $builder->addEventSubscriber($this->container->get('nitra.product.subscriber'));

        $this->modifyIsActiveField($builder);

        $builder->remove('model');
        $builder->remove('translations');
        $builder->remove('description');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\ProductBundle\\Document\\Product',
            'translation_domain' => 'NitraProductBundleProduct',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'model_products';
    }

    /**
     * Modify is active field to checkbox
     *
     * @param FormBuilderInterface $builder
     */
    protected function modifyIsActiveField($builder)
    {
        if ($builder->has('isActive')) {
            $articleOptions = $builder->get('isActive')->getOptions();
            unset (
                $articleOptions['choices'],
                $articleOptions['choice_list'],
                $articleOptions['empty_value'],
                $articleOptions['expanded'],
                $articleOptions['multiple'],
                $articleOptions['preferred_choices']
            );
            $builder->add('isActive', 'checkbox', $articleOptions);
        }
    }
}
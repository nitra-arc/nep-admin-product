<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;

class BrandFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'query_builder' => function(DocumentRepository $repository) {
                return $repository->createQueryBuilder()->sort('name', 'asc');
            },
            'class' => 'Nitra\ProductBundle\Document\Brand',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'brand_filter';
    }
}
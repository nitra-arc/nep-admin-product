<?php

namespace Nitra\ProductBundle\Form\Type\Translations;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\Container;

class NlTranslationFieldsType extends AbstractType
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['fields'] as $field => $option) {
            if (key_exists('type', $option)) {
                $type = $option['type'];
                unset($option['type']);
            } else {
                $type = 'text';
            }
            $builder->add($field, $type, array_merge(array(), $option))->get($field);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'fields'    => array(),
        ));
    }

    public function getName()
    {
        return 'nl_translation_fields';
    }
}
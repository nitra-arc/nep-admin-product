<?php

namespace Nitra\ProductBundle\Form\Type\Translations;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\Container;
use Nitra\ProductBundle\Form\EventListener\TranslatableSubscriber;

class NlTranslationType extends AbstractType
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dLocale    = array($this->container->getParameter('locale'));
        $locales    = $this->container->hasParameter('locales') ? $this->container->getParameter('locales') : $dLocale;
        foreach ($locales as $locale) {
            $builder->add($locale, new NlTranslationFieldsType($this->container), array(
                'fields'    => $options['fields'],
                'label'     => $locale,
            ));
        }
        $builder->addEventSubscriber(new TranslatableSubscriber($this->container));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'mapped'            => false,
            'fields'            => array(),
        ));
    }

    public function getName()
    {
        return 'nl_translation';
    }
}
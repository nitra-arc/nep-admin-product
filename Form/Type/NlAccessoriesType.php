<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class NlAccessoriesType extends AbstractCategoricalType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'preSubmit'));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'preSetData'));
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['categories'] = $this->getCategoriesHierarchyChild($options['cat_class']);
        $view->vars['cat_label'] = $options['cat_label'];
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class'       => 'Nitra\ProductBundle\Document\Product',
            'cat_label'   => '',
            'cat_class'   => 'Nitra\ProductBundle\Document\Category',
            'required'    => false,
            'multiple'    => true,
            'mapped'      => false,
            'choices'     => array(),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nl_accessories';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'nl_choice_double_list';
    }

    /**
     * Pre set data to form event handler
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $product = $event->getForm()->getParent()->getData();
        if (!$product) {
            return;
        }

        $choices = array();

        $items   = $product->{'get' . ucfirst($event->getForm()->getName())}();
        foreach ($items as $item) {
            $choices[$item->getId()] = (string) $item;
        }

        $event->setData($choices);
    }

    /**
     * Pre submit form event handler
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $product = $event->getForm()->getParent()->getData();

        $ids = $event->getData() ? : array();

        $items = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('id')->in($ids)
            ->getQuery()->execute()->toArray();

        $method = 'set' . ucfirst($event->getForm()->getName());
        $product->{$method}($items);

        $event->setData(null);
    }
}
<?php

namespace Nitra\ProductBundle\Form\Type\Model;

use Admingenerated\NitraProductBundle\Form\BaseModelType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class EditType extends BaseEditType
{
    /**
     * @var \Symfony\Component\EventDispatcher\EventSubscriberInterface[]
     */
    protected $subscribers;

    /**
     * Constructor
     * @param \Symfony\Component\EventDispatcher\EventSubscriberInterface[] $subscribers
     */
    public function __construct(array $subscribers)
    {
        $this->subscribers = $subscribers;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        foreach ($this->subscribers as $subscriber) {
            $builder->addEventSubscriber($subscriber);
        }
    }

    /**
     * @param \Symfony\Component\Form\FormView $view
     * @param \Symfony\Component\Form\FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $commonProductsFields = $form->get('commonProductsFields')->getData();

        $view->vars['commonProductsFields'] = $commonProductsFields;

        if (!$form->isSubmitted()) {
            $form->remove('commonProductsFields');
        }
    }
}
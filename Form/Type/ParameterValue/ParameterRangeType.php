<?php

namespace Nitra\ProductBundle\Form\Type\ParameterValue;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParameterRangeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('from', 'number', array(
            'label'    => 'fields.parameterRanges.from',
            'required' => false,
            'attr'     => array(
                'class' => 'number',
            ),
        ));
        $builder->add('to', 'number', array(
            'label'    => 'fields.parameterRanges.to',
            'required' => false,
            'attr'     => array(
                'class' => 'number',
            ),
        ));
        $builder->add('sortOrder', 'integer', array(
            'label'    => 'fields.parameterRanges.sortOrder',
            'required' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'parameter_ranges';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'error_bubbling' => false,
        ));
    }
}
<?php

namespace Nitra\ProductBundle\Form\Type\ParameterValue;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class ParameterValuesType extends AbstractType
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /**
     * Constructor
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container    = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'label'    => 'fields.parameterValues.subfields.name.label',
            'help'     => 'fields.parameterValues.subfields.name.help',
            'required' => true,
        ));

        if ($this->container->hasParameter('locales') && $this->container->getParameter('locales')) {
            $this->appendTranslationsFields($builder);
        }

        $builder->add('sortOrder', 'integer', array(
            'label'    => 'fields.parameterValues.subfields.sortOrder.label',
            'help'     => 'fields.parameterValues.subfields.sortOrder.help',
            'required' => false,
        ));
        $builder->add('isPopular', 'checkbox', array(
            'label'    => 'fields.parameterValues.subfields.isPopular.label',
            'help'     => 'fields.parameterValues.subfields.isPopular.help',
            'required' => false,
        ));
    }

    /**
     * Add additional fields for fill name in other languages
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     */
    protected function appendTranslationsFields($builder)
    {
        $translator = $this->container->get('translator');
        $defaultLocale = $this->container->getParameter('locale');
        $locales = $this->container->getParameter('locales');
        foreach ($locales as $locale) {
            if ($locale != $defaultLocale) {
                $builder->add($locale, 'text', array(
                    'required'           => false,
                    'mapped'             => false,
                    'help'               => $translator->trans(
                        'locales.' . $locale,
                        array(),
                        'NitraProductBundle'
                    ),
                    'label'              => 'fields.parameterValues.subfields.name.label',
                    'translation_domain' => 'NitraProductBundleParameter',
                ));
            }
        }
        $builder->addEventListener(FormEvents::POST_SET_DATA, array($this, 'postSetDataSaveTranslations'));
    }

    /**
     * Post set data form event handler
     * @param FormEvent $e
     */
    public function postSetDataSaveTranslations(FormEvent $e)
    {
        if ($e->getData()) {
            $data = $this->container->get('doctrine_mongodb.odm.document_manager')
                ->createQueryBuilder('Gedmo\Translatable\Document\Translation')
                ->hydrate(false)
                ->field('foreignKey')->equals($e->getData()->getId())
                ->field('objectClass')->equals('Nitra\\ProductBundle\\Document\\ParameterValue')
                ->sort('locale', 'asc')
                ->getQuery()->execute()->toArray();

            if ($data && is_array($data) && count($data)) {
                foreach ($data as $row) {
                    if (($row['field'] == 'name') && $e->getForm()->has($row['locale'])) {
                        $e->getForm()->get($row['locale'])->setData($row['content']);
                    }
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'parameter_values';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\\ProductBundle\\Document\\ParameterValue',
            'translation_domain'    => 'NitraProductBundleParameter',
            'error_bubbling'        => false,
        ));
    }
}
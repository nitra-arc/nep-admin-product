<?php

namespace Nitra\ProductBundle\Form\Type\Parameter;

use Admingenerated\NitraProductBundle\Form\BaseParameterType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\ProductBundle\Form\EventListener\ParameterValidator;

class NewType extends BaseNewType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventSubscriber(new ParameterValidator());
    }
}
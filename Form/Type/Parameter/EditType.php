<?php

namespace Nitra\ProductBundle\Form\Type\Parameter;

use Admingenerated\NitraProductBundle\Form\BaseParameterType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\ProductBundle\Form\EventListener\ParameterValidator;

class EditType extends BaseEditType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventSubscriber(new ParameterValidator());

        $parameter = $builder->getData();

        if ($parameter->getValueType() == 'text') {
            $builder->get('valueType')->setDisabled(true);
            $builder->remove('parameterRanges');
            $builder->remove('parameterValues');
        }
    }
}
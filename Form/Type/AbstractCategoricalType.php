<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractCategoricalType extends AbstractType
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container    = $container;
        $this->dm           = $container->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * Получение дерева категорий, с возможностью выбора только конечной
     * @param string $class
     * @return array
     */
    protected function getCategoriesHierarchyChild($class)
    {
        $qb = $this->dm->createQueryBuilder($class)
            ->sort('level', 'asc')
            ->sort('sortOrder', 'asc');
        $categories = $qb->getQuery()->execute();

        $result = array();
        foreach ($categories as $category) {
            if ($category->getLevel() > 1) {
                $this->recursiveAddCategory($category, $result);
            } else {
                $result[] = array(
                    'id'        => $category->getId(),
                    'text'      => $category->getName(),
                );
            }
        }

        return $result;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $category
     * @param array $result
     */
    protected function recursiveAddCategory($category, &$result)
    {
        foreach ($result as &$res) {
            if ($res['id'] == $category->getParent()->getId()) {
                if (!key_exists('children', $res)) {
                    $res['children'] = array();
                }
                $res['children'][] = array(
                    'id'        => $category->getId(),
                    'text'      => $category->getName(),
                );
                return true;
            } elseif (key_exists ('children', $res) && $res['children']) {
                if ($this->recursiveAddCategory($category, $res['children'])) {
                    return;
                }
            }
        }
    }
}
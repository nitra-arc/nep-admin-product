<?php

namespace Nitra\ProductBundle\Form\Type\Product;

use Admingenerated\NitraProductBundle\Form\BaseProductType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;

class FiltersType extends BaseFiltersType
{
    protected $statusOptions;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        if ($builder->has('status')) {
            if (key_exists('choices', $this->statusOptions)) {
                unset($this->statusOptions['choices']);
            }
            if (key_exists('empty_value', $this->statusOptions)) {
                unset($this->statusOptions['empty_value']);
            }
            $builder->add('status', 'checkbox', $this->statusOptions);
        }
    }

    /**
     * @param string $name
     * @param array $formOptions
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        if ($name == 'status') {
            $this->statusOptions = $formOptions;
        }

        return $formOptions;
    }
}
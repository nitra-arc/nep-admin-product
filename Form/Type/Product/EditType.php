<?php

namespace Nitra\ProductBundle\Form\Type\Product;

use Admingenerated\NitraProductBundle\Form\BaseProductType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EditType extends BaseEditType
{
    /**
     * @var EventSubscriberInterface[]
     */
    protected $subscribers;

    /**
     * @param EventSubscriberInterface[] $subscribers
     */
    public function __construct(array $subscribers)
    {
        $this->subscribers = $subscribers;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        foreach ($this->subscribers as $suscriber) {
            $builder->addEventSubscriber($suscriber);
        }
    }
}
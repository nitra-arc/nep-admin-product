<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class NlChoiceDoubleListType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $selected   = array();
        $unselected = array();
        foreach ($options['configs'] as $key => $config) {
            switch ($key) {
                case 'height':
                    $selected['height']      = $config . 'px';
                    $unselected['height']    = $config . 'px';
                    break;
                case 'selected-width':
                    $selected['min-width']   = $config . 'px';
                    break;
                case 'unselected-width':
                    $unselected['min-width'] = $config . 'px';
                    break;
            }
        }

        $view->vars['selected_styles']   = $selected   ? (preg_replace('/=/', ': ', http_build_query($selected, '', '; ')))   : null;
        $view->vars['unselected_styles'] = $unselected ? (preg_replace('/=/', ': ', http_build_query($unselected, '', '; '))) : null;

        parent::buildView($view, $form, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'multiple'  => true,
            'attr'      => array(
                'class'     => 'hidden-select',
            ),
            'configs'   => array(),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nl_choice_double_list';
    }
}
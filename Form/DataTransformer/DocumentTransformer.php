<?php

namespace Nitra\ProductBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\ODM\MongoDB\DocumentManager;

class DocumentTransformer implements DataTransformerInterface
{
    /**
     * @var DocumentManager
     */
    protected $dm;
    protected $class;
    protected $multiple;

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param type $class
     * @param type $multiple
     */
    public function __construct(DocumentManager $dm, $class, $multiple)
    {
        $this->dm       = $dm;
        $this->class    = $class;
        $this->multiple = $multiple;
    }

    /**
     * @return string
     */
    public function transform($obj)
    {
        if (null === $obj) {
            return $this->multiple ? array() : null;
        }

        if ($this->multiple) {
            $result = array();
            foreach ($obj as $is) {
                $result[] = $is->getId();
            }
            return $result;
        } else {
            return is_object($obj) ? $obj->getId() : $obj;
        }
    }

    /**
     * Transforms a string|number|array to an object (issue).
     * @param string|number|array $id
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return $this->multiple ? array() : null;
        }

        $results = $this->multiple ? array() : null;
        if (is_array($id)) {
            // Получаем все категории отсортированные по уровню
            $parent     = array();
            $categories = $this->dm->createQueryBuilder('NitraProductBundle:Category')
                ->field('id')->in($id)
                ->sort('level', 1)
                ->getQuery()
                ->execute();

            foreach ($categories as $k => $c) {
                // first level, add category in result
                if ($c->getLevel() == '1') {
                    $parent[]     = $c->getId();
                    $results[]    = $c;
                } else {
                    // проверяем на наличие родителя в массиве
                    if(!$parent || ($parent && !$this->inParentCategory($c->getParent(), $parent)) ){
                        $parent[]   = $c->getId();
                        $results[]  = $c;
                    }
                }
            }
        } else {
            $results = $this->dm->find('NitraProductBundle:Category', $id);
        }

        return $results;
    }

    protected function inParentCategory($category, $parent)
    {
        if (in_array($category->getId(), $parent)) {
            return true;
        }

        if ($category->getLevel() > 1) {
            return $this->inParentCategory($category->getParent(), $parent);
        }

        return false;
    }
}
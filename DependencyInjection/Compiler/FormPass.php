<?php

namespace Nitra\ProductBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FormPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');
        $resources[] = 'NitraProductBundle:Form:fields.html.twig';
        $resources[] = 'NitraProductBundle:Form:nllocaler.html.twig';
        $resources[] = 'NitraProductBundle:Form:treeCategory.html.twig';
        $resources[] = 'NitraProductBundle:Form:modelProducts.html.twig';
        $resources[] = 'NitraProductBundle:Form:accessories.html.twig';

        $container->setParameter('twig.form.resources', $resources);
    }
}
<?php

namespace Nitra\ProductBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $builder->root('nitra_product')
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('search_listener')
                ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('fields')
                        ->prototype('variable')->end()
                        ->defaultValue(array(
                            array(
                                'getModel',
                                'getCategory',
                                'getName',
                            ),
                            array(
                                'getModel',
                                'getBrand',
                                'getName',
                            ),
                            array(
                                'getModel',
                                'getName',
                            ),
                            'getName',
                            'getArticle',
                        ))
                        ->end()
                    ->end()
                    ->children()
                        ->arrayNode('instanceof')
                        ->prototype('scalar')->end()
                        ->defaultValue(array(
                            'category'  => '\Nitra\ProductBundle\Document\Category',
                            'brand'     => '\Nitra\ProductBundle\Document\Brand',
                        ))
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $builder;
    }
}
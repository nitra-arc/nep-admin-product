<?php

namespace Nitra\ProductBundle\QueryFilter;

use Admingenerator\GeneratorBundle\QueryFilter\DoctrineODMQueryFilter as BaseDoctrineODMQueryFilter;
use Symfony\Component\DependencyInjection\Container;

class NitraDoctrineODMQueryFilter extends BaseDoctrineODMQueryFilter
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->dm           = $container->get('doctrine_mongodb.odm.document_manager');
    }

    public function addCollectionFilter($field, $value)
    {
         $this->addDocumentFilter($field, $value);
    }

    public function addDocumentFilter($field, $value)
    {
        $ids = $this->transformValueToValues($value);
        $this->query->field($field.'.$id')->in($ids);
    }

    protected function transformValueToValues($value)
    {
        $this->container->get('logger')->warning('asasas' . get_class($value) . '----' . (($value instanceof \Nitra\ProductBundle\Document\Category) ? 'true'  : 'false') . '---' . $value->getName());
        if ($value instanceof \Nitra\ProductBundle\Document\Category) {
            $path = preg_quote($value->getPath());
            return $this->dm->createQueryBuilder('NitraProductBundle:Category')
                ->distinct('_id')
                ->field('path')->equals(new \MongoRegex("/^{$path}/i"))
                ->field('level')->gte($value->getLevel())
                ->getQuery()->execute()->toArray();
        } else {
            return array(
                new \MongoId($value->getId()),
            );
        }
    }
}